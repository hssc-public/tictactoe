function add(x, y) {
    return x + y;
}

function multiply(x, y) {
    return x * y;
}

let multiplyFunction = multiply;

let subtract = function (x, y) {
    return x - y;
};


console.log(multiplyFunction(3, 4));

console.log('Add 2 and 3', add(2, 3));
console.log('Multiply 2 and 3', multiply(2, 3));
console.log('Substract 2 and 3', subtract(2, 3));