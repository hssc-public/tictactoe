posts = [
    {
        id: 'post01',
        image: {
            src: 'http://placebear.com/300/300',
            alt: 'Bears'
        },
        title: 'First post',
        lead: 'This is the lead of the first post'
    },
    {
        id: 'post02',
        image: {
            src: 'http://placebear.com/300/300',
            alt: 'Bears'
        },
        title: 'Second post',
        lead: 'This is the lead of the first post'
    },
    {
        id: 'post03',
        image: {
            src: 'http://placebear.com/300/300',
            alt: 'Bears'
        },
        title: 'Third post',
        lead: 'This is the lead of the first post'
    },
    {
        id: 'post04',
        image: {
            src: 'images/panda.jpg',
            alt: 'Bears'
        },
        title: 'Fourth post',
        lead: 'This is the lead of the first post'
    },
];

for(let post of posts){
    console.log(post.title);
}


let postTitles = [];
posts.forEach(function (value, index, array) {
    postTitles.push(value.title);
});

let postTitlesWithMap = posts.map(post => ({title: post.title, imageSource: post.image.src}));

let filterFn = function (post) {
    return !post.image.src.includes('http');
}

let filterLambda = (post) => filterFn(post);

function filter(value, index, array) {
    return !value.image.src.includes('http')
}

let filtered = posts
    .filter(filter)
    .map(post => ({title: post.title, imageSource: post.image.src}));
console.log('------');
console.log(postTitlesWithMap);
console.log('------');
console.log(postTitles);
console.log('------');
console.log(filtered);
