class Board {
    constructor() {
        this.fields = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ];
    }

    place(x, y, mark) {
        checkBound(x, 'X', 0, 2);
        checkBound(y, 'Y', 0, 2);
        if (!this.fields[x][y]) {
            this.fields[x][y] = mark;
        }
    }

    fieldEmpty(x, y) {
        return !this.fields[x][y] ? true : false;
    }

    getAllWinningPossibilites() {
        let rows = this.fields;
        let columns = [];
        for (let i = 0; i < this.fields.length; i++) {
            columns.push([
                this.fields[0][i],
                this.fields[1][i],
                this.fields[2][i]
            ]);
        }
        return rows.concat(columns, [
            [this.fields[0][0], this.fields[1][1], this.fields[2][2]],
            [this.fields[0][2], this.fields[1][1], this.fields[2][0]]
        ]);
    }

    * getRows() {
        for (let row of this.fields) {
            yield {data: row, type: 'row'};
        }
    }

    * getColumns() {
        for (let i = 0; i < this.fields.length; i++) {
            yield {
                data: [
                    this.fields[0][i],
                    this.fields[1][i],
                    this.fields[2][i]
                ],
                type: 'column'
            };
        }
    }

    * getPossibilities() {
        yield* this.getRows();
        yield* this.getColumns();
    }

    [Symbol.iterator]() {
        return this.getPossibilities();
    }
}


class TicTacToe {
    constructor() {
        this.board = new Board();
        this.currentPlayer = 'X';
    }

    placeMark(x, y) {
        if (this.board.fieldEmpty(x, y)) {
            this.board.place(x, y, this.currentPlayer);
            this.changePlayer();
        }
        let winner = this.whoWins();
        if(winner){
            return 'Return the winner is ' + winner;
        } else {
            //check for tie
            return 'The next player is' + this.currentPlayer;
        }
    }

    changePlayer() {
        this.currentPlayer = this.currentPlayer === 'X' ? 'O' : 'X';
    }

    whoWins(){
        for (let triplets of this.board) {
            const result = triplets.data.filter((value) => !!value).reduce((accumulator, value) =>{
                accumulator[value]++;
                return accumulator;
            }, {X: 0, O:0});
            if(result.X === 3){
                return 'X';
            } else if(result.O === 3){
                return 'O';
            }
        }
        return '';
    }
}


let game = new TicTacToe();

function checkBound(value, name, min, max) {
    if (typeof value !== 'number' || value < min || value > max) {
        throw new Error(`${name} must be positive number and not greater than ${max}`);
    }
}

console.log(typeof game);
console.log(game.board instanceof Array);
console.log(Array.isArray(game.board));

try {
    game.placeMark(1, 1);
    game.placeMark(0, 2);
    game.placeMark(1, 2);
    console.log(game.placeMark(0, 1));;
    console.log(game.placeMark(1, 0));
} catch (e) {
    console.log('Error ', e.message);
}
finally {
    console.log('Finally');
    console.log(game.board);
}

for (let pos of game.board) {
    console.log('iterator', pos);
}